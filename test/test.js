'use strict';
process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const expect = chai.expect;
let accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjcsInVzZXJuYW1lIjoidGVzdFRlc3QiLCJpYXQiOjE1NjY2NzQwMjV9.PeYW_75oUPOKzTxiEI4CGJJDdGG5Ppldu7iQnRXgygE';
chai.use(chaiHttp);

describe('User Controller Test', function () {
    it('Should Save Data Into Db ', function () {
        return chai.request(app)
            .post('/users/')
            .send(
                {
                    "firstname": "Test",
                    "lastname": "Test1",
                    "username": "testTest1",
                    "email": "test1@test.com",
                    "password": "test123"

                })
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('message').eql('Account Created Successfully');
                expect(res.body).to.have.property('data');
                expect(res).to.be.json;
            }).catch(function (err) {
                expect(err).to.have.status(200);
                expect(res.body).to.have.property('message');
            });
    });

    it('Should Retrieve Access Token And User Data', function () {
        return chai.request(app)
            .post('/users/login')
            .send({
                "email": "test1@test.com",
                "password": "test123"
            })
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('message').eql('Logged In Successfully');
                expect(res.body).to.have.property('data');
                expect(res.body.data).to.have.property('accessToken');
                expect(res).to.be.json;
            }).catch(function (err) {
                expect(err).to.have.status(200);
                expect(res.body).to.have.property('message');
            });
    });

    it('Should Return User Profile', function () {
        return chai.request(app)
            .get('/users/testTest')
            .set('Authorization', accessToken)
            .then(function (res) {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property('message').eql('User Profile getted successfully');
                expect(res.body).to.have.property('data');
            }).catch(function (err) {
                expect(err).to.have.status(404);
                expect(res.body).to.have.property('message').eql('User Not Found');
                expect(err).to.have.status(400);
                expect(res.body).to.have.property('message').eql('Token not found in request headers');
            });
    });
})
