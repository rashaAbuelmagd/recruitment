const models = require('../models/')
const Sequelize = require('sequelize')

exports.getAllUserNames = async (userNames) => {

    const users = await models.User.findAll({
        raw: true,
        attributes: ["username", "email"],
        where: {
            username: {
                [Sequelize.Op.in]: userNames
            }
        }
    })
    return users

}

exports.getByUserName = async (username) => {

    const user = await models.User.findOne({
        attributes: ["firstname", "lastname", "username", "email"],
        where:
        {
            'username': username
        }
    })
    return user

}

exports.getByEmail = async (email) => {

    return await models.User.findOne({ where: { email: email } })

}

exports.store = async (userData) => {

    return await models.User.create(userData)

}