FROM node:10.16.0-alpine

ENV HOST=0.0.0.0

RUN apk --no-cache add --virtual builds-deps build-base python

COPY . .

# Enter api dir
WORKDIR .

# Create empty .env file
RUN touch .env && \
    npm install && \
    npm install pm2 npx apidoc -g

# Start server
CMD npx sequelize-cli db:migrate && \
    apidoc -i controllers/ -o public/docs/ && \
    pm2-runtime start ecosystem.config.js