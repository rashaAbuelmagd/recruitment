var express = require('express');
var router = express.Router();

const userController = require('../controllers/userController');
const middleware = require('../middlewares/Authenticate');
const userValidator = require('../validations/user');

router.post('/', userValidator.validate('create'), userController.create);
router.post('/login', userController.login);
router.get('/:username', middleware.Authenticate, userController.get);

module.exports = router;
