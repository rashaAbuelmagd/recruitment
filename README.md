# recruitment


## Installation

Clone the repo, enter the app dir and run the following commands:

```bash
cp .env.dev .env
docker-compose up -d --build
```

## Usage
- To create a new user or post please follow the [API docs](http://localhost:3339/docs).

## Test
- To Run Unit Test 

```bash
docker-compose exec api npm test
```

## Content and technologies

* BackEnd Service : node js - Express
* Unit Test : mocha - chai - chai-http - chai expect
* Database : postgres
* Api Document : apidoc
* Docker and Docker compose for Database and back end service


## Note

- Please Add an email and password in the **.env file**  to be the From Email, for sending emails from it.