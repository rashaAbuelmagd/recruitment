
module.exports = {
  apps: [{
    name: 'recruitment',
    script: 'bin/www',
    instances: 1,
    autorestart: true,
    max_memory_restart: '1G'
  }]
}