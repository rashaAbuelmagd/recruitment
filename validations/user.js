const { check } = require('express-validator')
const models = require('../models/')

exports.validate = (method) => {
    switch (method) {
        case 'create': {
            return [
                check('firstname').not().isEmpty().withMessage('please enter your first name'),

                check('lastname').not().isEmpty().withMessage('please enter your last name'),

                check('username', 'your username already in use')
                    .custom(value => {
                        return models.User.findOne({where : {"username": value}}).then(user => {
                            if (user) {
                                return Promise.reject('username already in use');
                            }
                        })
                    })
                    .not().isEmpty().withMessage('Please Enter username'),

                check('email')
                    .custom(value => {
                        return models.User.findOne({where : {"email": value}}).then(user => {
                            if (user) {
                                return Promise.reject('email already in use');
                            }
                        })
                    }).isEmail().withMessage('Please Enter valid email')
                    .not().isEmpty().withMessage('Please Enter your email'),

                check('password').not().isEmpty().isLength({ min: 5 }).withMessage('please enter your password at least 5 characters')
            ]
        }
        case 'login': {
            return [
                check('email')
                    .isEmail().withMessage('Please Enter valid email')
                    .not().isEmpty().withMessage('Please Enter your email'),

                check('password').not().isEmpty().isLength({ min: 5 }).withMessage('please enter your password')
            ]
        }
    }
}